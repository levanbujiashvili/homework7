package com.example.davaleba7

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycler_item_layout.view.*

class MyAdapter(private val context: Context, private val myList: MutableList<ItemModel>):RecyclerView.Adapter<MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView = LayoutInflater.from(context).inflate(R.layout.recycler_item_layout, parent , false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return myList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso.get().load(myList[position].cover).into(holder.myImage)
        holder.myTitle.text = myList[position].titleEN
        holder.description.text = myList[position].descriptionEN
    }


}