package com.example.davaleba7

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_item_layout.view.*


class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var myImage: ImageView
    var myTitle: TextView
    var description: TextView

    init {
        myImage = itemView.myImage
        myTitle = itemView.myTitle
        description = itemView.description
    }

}
