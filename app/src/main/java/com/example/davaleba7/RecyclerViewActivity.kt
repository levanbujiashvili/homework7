package com.example.davaleba7

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.davaleba7.databinding.RecyclerItemLayoutBinding
import com.example.davaleba7.retrofit.Common
import com.example.davaleba7.retrofit.RetrofitService
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecyclerViewActivity: AppCompatActivity(){
    private lateinit var adapter:MyAdapter
    lateinit var layoutManager: LinearLayoutManager
    lateinit var dialog: AlertDialog
    lateinit var mService : RetrofitService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val listItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.recycler_item_layout, viewGroup, false)
        listItemBinding.itemModel = ItemModel()
        mService = Common.retrofitService
        layoutManager = LinearLayoutManager(this)
        myRecyclerView.setHasFixedSize(true)
        myRecyclerView.layoutManager = layoutManager
        dialog = SpotsDialog.Builder().setCancelable(false).setContext(this).build()
        getAllHouseList()
    }
    private fun getAllHouseList() {
        dialog.show()
        mService.getHouses().enqueue(object : Callback<MutableList<ItemModel>> {
            override fun onFailure(call: Call<MutableList<ItemModel>>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<MutableList<ItemModel>>,
                response: Response<MutableList<ItemModel>>
            ) {
                adapter = MyAdapter(baseContext, response.body() as MutableList<ItemModel>)
                adapter.notifyDataSetChanged()
                myRecyclerView.adapter = adapter
                dialog.dismiss()
            }

        })
    }
}