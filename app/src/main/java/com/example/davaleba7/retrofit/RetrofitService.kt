package com.example.davaleba7.retrofit

import com.example.davaleba7.ItemModel
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {
        @GET("5edb4d643200002a005d26f0")

        fun getHouses(): Call<MutableList<ItemModel>>
}