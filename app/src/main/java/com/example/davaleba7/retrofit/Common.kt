package com.example.davaleba7.retrofit

object Common {
    private val BASE_URL = "www.mocky.io/v2/"

    val retrofitService: RetrofitService
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitService::class.java)
}